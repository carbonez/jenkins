#!/usr/bin/env bash
set -e

# Install docker client via binaries
wget -q https://download.docker.com/linux/static/stable/$(uname -m)/docker-${DOCKER_VERSION}.tgz
tar xzf docker-${DOCKER_VERSION}.tgz
mv docker/docker /usr/local/bin/docker
chmod +x /usr/local/bin/docker
rm -rf docker*
