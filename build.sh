#!/usr/bin/env bash
set -e

source .env
docker login

# Jenkins master
docker build --build-arg DOCKER_VERSION=${DOCKER_VERSION} -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME}:latest ${IMAGE_NAME}:${DOCKER_VERSION}
docker push ${IMAGE_NAME}

# Jenkins slave
docker build --build-arg DOCKER_VERSION=${DOCKER_VERSION} -f Dockerfile.slave -t ${SLAVE_IMAGE_NAME} .
docker tag ${SLAVE_IMAGE_NAME}:latest ${SLAVE_IMAGE_NAME}:${DOCKER_VERSION}
docker push ${SLAVE_IMAGE_NAME}
