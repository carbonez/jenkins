FROM jenkins/jenkins:lts

USER root
ENV JENKINS_USER=jenkins

# Install docker client
ARG DOCKER_VERSION
ENV DOCKER_VERSION=${DOCKER_VERSION}

COPY install-docker.sh install-docker.sh
RUN chmod +x install-docker.sh \
 && ./install-docker.sh \
 && rm -f install-docker.sh

# Skip setup wizard
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

# Install plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

# Start as ${JENKINS_USER}
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh \
 && apt-get update \
 && apt-get install --no-install-recommends --yes sudo

ENTRYPOINT ["/entrypoint.sh"]
CMD sudo -E -H -u ${JENKINS_USER} env "PATH=$PATH" bash -c jenkins.sh
